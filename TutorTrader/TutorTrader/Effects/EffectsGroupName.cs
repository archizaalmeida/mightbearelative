﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

namespace TutorTrader.Effects
{
    public class EffectsGroupName
    {
        #region Fields

        ///<summary>
        ///    Unique resolution group name that is accessed by every effect.
        ///</summary>
        public const string ResolutionGroupName = "Effects";

        #endregion
    }
}