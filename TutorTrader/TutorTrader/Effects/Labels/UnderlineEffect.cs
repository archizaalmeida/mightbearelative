﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Xamarin.Forms;

namespace TutorTrader.Effects.Labels
{
    public class UnderlineEffect : RoutingEffect
    {
        #region Methods

        #region Constructors

        ///<summary>
        ///    Default Constructor.
        ///</summary>
        public UnderlineEffect() : base($"{EffectsGroupName.ResolutionGroupName}.{nameof(UnderlineEffect)}")
        {
        }

        #endregion

        #endregion
    }
}