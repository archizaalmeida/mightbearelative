﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Xamarin.Forms;

namespace TutorTrader.Effects.Entries
{
    public class HideUnderlineEffect : RoutingEffect
    {
        #region Methods

        #region Constructors

        ///<summary>
        ///    Default Constructor.
        ///</summary>
        public HideUnderlineEffect() : base($"{EffectsGroupName.ResolutionGroupName}.{nameof(HideUnderlineEffect)}")
        {
        }

        #endregion

        #endregion
    }
}