﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using TutorTrader.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TutorTrader.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecoverPasswordPage : ContentPage
    {
        #region Methods

        #region Constructors

        ///<summary>
        ///    Default Constructor.
        ///</summary>
        public RecoverPasswordPage()
        {
            InitializeComponent();
            BindingContext = new RecoverPasswordPageViewModel();
        }

        #endregion

        #endregion
    }
}