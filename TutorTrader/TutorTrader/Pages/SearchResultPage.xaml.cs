﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System.Collections.ObjectModel;
using TutorTrader.Model;
using TutorTrader.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TutorTrader.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchResultPage : ContentPage
    {
        public SearchResultPage(ObservableCollection<PersonToBeBaptized> people)
        {
            InitializeComponent();
            BindingContext = new SearchResultPageViewModel(this, people);
        }
    }
}