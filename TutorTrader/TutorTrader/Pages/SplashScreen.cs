﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Xamarin.Forms;

namespace TutorTrader.Pages
{
    public class SplashScreen : ContentPage
    {
        #region Fields

        private Image splashImage;

        #endregion

        #region Methods

        #region Constructors

        public SplashScreen()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            var sub = new AbsoluteLayout
            {
                BackgroundColor = Color.White
            };

            splashImage = new Image
            {
                Source = "Logo.png"
            };
            AbsoluteLayout.SetLayoutFlags(splashImage, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(splashImage,
                new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));

            sub.Children.Add(splashImage);
            BackgroundColor = Color.White;
            Content = sub;
        }

        #endregion

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await splashImage.ScaleTo(1, 1300);
            await splashImage.ScaleTo(0.8, 250, Easing.Linear);
            await splashImage.TranslateTo(splashImage.TranslationX, -Height - 50, 1500);
            Application.Current.MainPage = new NavigationPage(new LoginPage());
        }

        #endregion
    }
}