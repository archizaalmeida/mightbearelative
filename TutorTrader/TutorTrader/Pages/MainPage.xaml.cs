﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;
using TabbedPage = Xamarin.Forms.TabbedPage;

namespace TutorTrader.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : TabbedPage
    {
		public MainPage ()
		{
		    Xamarin.Forms.PlatformConfiguration.AndroidSpecific.TabbedPage.SetToolbarPlacement(On<Android>(), ToolbarPlacement.Bottom);

            var profilePage = new NavigationPage(new ProfilePage());
            profilePage.Icon = "user.png";
            profilePage.Title = "Profile";
            Children.Add(profilePage);

            var searchPage = new NavigationPage(new SearchPage());
            searchPage.Icon = "search.png";
            searchPage.Title = "Search";
            Children.Add(searchPage);

            var possibleRelativesPage = new NavigationPage(new PossibleRelativesPage());
            possibleRelativesPage.Icon = "about.png";
            possibleRelativesPage.Title = "Relatives";
            Children.Add(possibleRelativesPage);

            var aboutPage = new NavigationPage(new AboutPage());
            aboutPage.Icon = "about.png";
            aboutPage.Title = "About";
		    Children.Add(aboutPage);

            CurrentPage = Children[1];

            ToolbarItems.Add(new ToolbarItem("Logout", "logout.png", () =>
            {
                Navigation.PushModalAsync(new LoginPage());
            }));

        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}