﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using TutorTrader.Model;
using TutorTrader.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TutorTrader.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchPage : ContentPage
	{
		public SearchPage()
		{
			InitializeComponent();
            BindingContext = new SearchPageViewModel(this);
        }
	}
}