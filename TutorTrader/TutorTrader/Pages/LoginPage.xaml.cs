﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using TutorTrader.ViewModels;

namespace TutorTrader.Pages
{
    public partial class LoginPage
    {
        #region Methods

        #region Constructors

        ///<summary>
        ///    Default Constructor.
        ///</summary>
        public LoginPage()
        {
            InitializeComponent();
            BindingContext = new LoginPageViewModel(this);
        }

        #endregion

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        #endregion
    }
}