﻿using System.Collections.Generic;

namespace TutorTrader.Model.UserModel
{
    public class UsersModel
    {
        public User[] users { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public string contactName { get; set; }
        public string givenName { get; set; }
        public string familyName { get; set; }
        public string email { get; set; }
        public string country { get; set; }
        public string gender { get; set; }
        public string birthDate { get; set; }
        public string preferredLanguage { get; set; }
        public string displayName { get; set; }
        public string personId { get; set; }
        public string treeUserId { get; set; }
        public Link links { get; set; }


    }

    public class Link
    {
        public HrefObject person { get; set; }
        public HrefObject self { get; set; }
        public HrefObject artifacts { get; set; }
    }

    public class HrefObject
    {
        public string href { get; set; }
    }
}
