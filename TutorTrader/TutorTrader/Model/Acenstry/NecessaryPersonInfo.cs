﻿namespace TutorTrader.Model.Acenstry
{
    public class NecessaryPersonInfo
    {
        public string lastName { get; set; }
        public string birthPlace { get; set; }

        public NecessaryPersonInfo(string lastName, string birthPlace)
        {
            this.lastName = lastName;
            this.birthPlace = birthPlace;
        }
}
}
