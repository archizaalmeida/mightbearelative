﻿

namespace TutorTrader.Model.Acenstry
{
    public class AncestryResponse
    {
        public MainLinks links { get; set; }
        public Person[] persons { get; set; }

    }

    public class MainLinks
    {
        public Collection collection { get; set; }
    }

    public class Collection
    {
        public string href { get; set; }
    }

    public class Person
    {
        public string id { get; set; }
        public string sortKey { get; set; }
        public bool living { get; set; }
        public Gender gender { get; set; }
        public PersonLinks links { get; set; }
        public Identifiers identifiers { get; set; }
        public Names[] names { get; set; }
        public Fact[] facts { get; set; }
        public Display display { get; set; }
        public PersonInfo[] personInfo { get; set; }
    }

    public class Fact
    {
        public string id { get; set; }
        public Attribution attribution { get; set; }
        public string type { get; set; }
        public Date date { get; set; }
        public Place place { get; set; }
        public GenderNamesLinks links { get; set; }
        public Display display { get; set; }
    }

    public class Place
    {
        public string original { get; set; }
        public string description { get; set; }
        public Normalized[] normalized { get; set; }
    }

    public class Date
    {
        public string original { get; set; }
        public string formal { get; set; }
        public Normalized[] normalized { get; set; }
    }
    public class Normalized
    {
        public string lang { get; set; }
        public string value { get; set; }
    }

    public class Gender
    {
        public string id { get; set; }
        public Attribution attribution { get; set; }
        public string type { get; set; }
        public GenderNamesLinks links { get; set; }
    }

    public class GenderNamesLinks
    {
        public Conclusion conclusion { get; set; }
    }

    public class Conclusion
    {
        public string href { get; set; }
    }
    
    public class Attribution
    {
        public Contributor contributor { get; set; }
        public string modified { get; set; }
    }

    public class Contributor
    {
        public string resource { get; set; }
        public string resourceId { get; set; }
    }

    public class PersonLinks
    {
        public Collection spouses { get; set; }
        public Collection changehistory { get; set; }
        public Collection ancestry { get; set; }
        public Collection notes { get; set; }
        public Collection nonmatches { get; set; }
        public Collection portraits { get; set; }
        public Collection ordinances { get; set; }
        public Collection collection { get; set; }
        public Collection families { get; set; }
        public Collection portrait { get; set; }
        public Collection matches { get; set; }
        public Collection children { get; set; }
        public Collection descendancy { get; set; }
        public Collection person { get; set; }
        public Collection sourcedescriptions { get; set; }
        public Merge merge { get; set; }
        public Collection self { get; set; }
        public Collection ordinancereservations { get; set; }
        public Collection artifacts { get; set; }
        public Collection parents { get; set; }
    }

    public class Merge
    {
        public string template { get; set; }
        public string type { get; set; }
        public string accept { get; set; }
        public string allow { get; set; }
        public string title { get; set; }
    }

    public class Identifiers
    {
        public string[] http { get; set; }
        public string[] urn { get; set; }
    }

    public class Names
    {
        public string id { get; set; }
        public Attribution attribution { get; set; }
        public string type { get; set; }
        public bool preferred { get; set; }
        public GenderNamesLinks links { get; set; }
        public NameForms[] nameForms { get; set; }
    }

    public class NameForms
    {
        public string lang { get; set; }
        public string fullText { get; set; }
        public Part[] parts { get; set; }
        public NameFormInfo[] nameFormInfo { get; set; }
    }

    public class Part
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class NameFormInfo
    {
        public string order { get; set; }
    }

    public class Display
    {
        public string name { get; set; }
        public string gender { get; set; }
        public string lifespan { get; set; }
        public string birthDate { get; set; }
        public string birthPlace { get; set; }
        public string ascendancyNumber { get; set; }
        public string descendancyNumber { get; set; }
        public FamiliesAs[] familiesAsParent { get; set; }
        public FamiliesAs[] familiesAsChild { get; set; }
    }

    public class FamiliesAs
    {
        public Contributor parent1 { get; set; }
        public Contributor parent2 { get; set; }
        public Contributor[] children { get; set; }
    }

    public class PersonInfo
    {
        public bool readOnly { get; set; }
        public bool privateSpaceRestricted { get; set; }
    }
}
