﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TutorTrader.Model.Acenstry
{
    public class PersonResponse
    {
        public string description { get; set; }
        public Person[] persons { get; set; }
        public Relationships[] relationships { get; set; }
        public SourceDescriptions sourceDescriptions { get; set; }
        public PersonPlace[] places { get; set; }
        public ChildAndParentsRelationship[] childAndParentsRelationships { get; set; }
    }

    public class ChildAndParentsRelationship
    {
        public string id { get; set; }
        public Contributor parent1 { get; set; }
        public Contributor parent2 { get; set; }
        public Contributor child { get; set; }
        public ChildAndParentsLinks links { get; set; }
        public Identifiers identifiers { get; set; }
    }

    public class ChildAndParentsLinks
    {
        public Collection parent1 { get; set; }
        public Collection relationship { get; set; }
        public Collection parent2 { get; set; }
        public Collection child { get; set; }
    }

    public class PersonPlace
    {
        public string id { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public PlaceName[] names { get; set; }
    }

    public class PlaceName
    {
        public string lange { get; set; }
        public string value { get; set; }
    }

    public class SourceDescriptions{
        public string id { get; set; }
        public string about { get; set; }
        public ComponentOf componentOf { get; set; }
        public string resourceType { get; set; }
        public string modified { get; set; }
        public string version { get; set; }
        public SourceLinks links { get; set; }
        public Citation[] citations { get; set; }
        public Title[] titles { get; set; }
        public Identifiers identifiers { get; set; }
    }

    public class Title
    {
        public string value { get; set; }
    }

    public class Citation
    {
        public string lang { get; set; }
        public string value { get; set; }
    }

    public class SourceLinks
    {
        public Collection description { get; set; }
    }

    public class ComponentOf
    {
        public string description { get; set; }
    }

    public class Relationships
    {
        public string id { get; set; }
        public string sortKey { get; set; }
        public string type { get; set; }
        public Contributor person1 { get; set; }
        public Contributor person2 { get; set; }
        public RelationshipsLinks links { get; set; }
        public Collection identifiers { get; set; }

    }

    public class RelationshipsLinks
    {
        public Collection person2 { get; set; }
        public Collection person1 { get; set; }
        public Collection relationship { get; set; }
    }
}
