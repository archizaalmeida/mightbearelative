﻿

using TutorTrader.Model.Acenstry;
using TutorTrader.Model.UserModel;

namespace TutorTrader.Model.Search
{
    public class SearchResponse
    {
        public int results { get; set; }
        public Link links { get; set; }
        public Entries[] entries { get; set; }

    }

    public class Link
    {
        public HrefObject next { get; set; }
        public HrefObject self { get; set; }
        public HrefObject collection { get; set; }
        public HrefObject person { get; set; }
    }

    public class Entries
    {
        public EntriesContent content { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public double score { get; set; }
        public Link links { get; set; }
        public MatchInfo[] matchInfo { get; set; }
    }

    public class MatchInfo
    {
        public string collection { get; set; }
    }

    public class EntriesContent
    {
        public Gedcomx gedcomx { get; set; }
    }
    public class Gedcomx
    {
        public Link links { get; set; }
        public Person[] persons { get; set; }
        public Places[] places { get; set; }
    }

    public class Places
    {
        public string id { get; set; }
        public string lang { get; set; }
        public string type { get; set; }
        public TemporalDescription temporalDescription { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public Jurisdiction jurisdiction { get; set; }
        public Identifiers identifiers { get; set; }
        public Name[] names { get; set; }
        public Display display { get; set; }
        public PlaceDescriptionInfo[] placeDescriptionInfo { get; set; }
    }

    public class PlaceDescriptionInfo
    {
        public int zoomLevel { get; set; }
    }

    public class Name
    {
        public string lang { get; set; }
        public string value { get; set; }
    }
    public class Display
    {
        public string name { get; set; }
        public string fullName { get; set; }
        public string type { get; set; }
    }
    public class Jurisdiction
    {
        public string resource { get; set; }
        public string resourceId { get; set; }
    }

    public class TemporalDescription
    {
        public string formal { get; set; }
    }

}