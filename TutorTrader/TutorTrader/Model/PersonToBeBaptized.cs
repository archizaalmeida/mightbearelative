﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TutorTrader.Properties;

namespace TutorTrader.Model
{
    public class PersonToBeBaptized
    {
        private string _name;
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }
        private string _age;
        public string Age
        {
            get => _age;
            set => SetProperty(ref _age, value);
        }
        private string _personId;
        public string PersonID
        {
            get => _personId;
            set => SetProperty(ref _personId, value);
        }
        private bool _baptized;
        public bool Baptized
        {
            get => _baptized;
            set => SetProperty(ref _baptized, value);
        }
        private bool _dead;
        public bool Dead
        {
            get => _dead;
            set => SetProperty(ref _dead, value);
        }
        private string _gender;
        public string Gender
        {
            get => _gender;
            set => SetProperty(ref _gender, value);
        }
        private string _birthPlace;
        public string BirthPlace
        {
            get => _birthPlace;
            set => SetProperty(ref _birthPlace, value);
        }
        private string _deathDate;
        public string DeathDate
        {
            get => _deathDate;
            set => SetProperty(ref _deathDate, value);
        }
        private string _deathPlace;
        public string DeathPlace
        {
            get => _deathPlace;
            set => SetProperty(ref _deathPlace, value);
        }

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
