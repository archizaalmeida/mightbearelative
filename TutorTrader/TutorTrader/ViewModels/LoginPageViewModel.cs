﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Models;
using Xamarin.Forms;
using TutorTrader.Pages;
using TutorTrader.Properties;
using TutorTrader.Libraries;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The Login view model class.
    /// </summary>
    /// <seealso cref="INotifyPropertyChanged" />
    public class LoginPageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;

        private Page _page;
        private User _userModel = new User();
        private Boolean _acceptedTerms;
        private UserController _userController = UserController.Instance;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets user information.
        /// </summary>
        public User UserModel
        {
            get => _userModel;
            set  { _userModel = value; OnPropertyChanged(nameof(UserModel)); }// SetProperty(ref _userModel, value);
        }

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public Boolean IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        /// <summary>
        ///     Gets/sets page accepted terms.
        /// </summary>
        public Boolean AcceptedTerms
        {
            get => _acceptedTerms;
            set => SetProperty(ref _acceptedTerms, value);
        }

        /// <summary>
        ///     Gets/sets login command.
        /// </summary>
        public Command LoginCommand { get; set; }

        /// <summary>
        ///     Gets/sets register command.
        /// </summary>
        public Command RegisterCommand { get; set; }

        /// <summary>
        ///     Gets/sets forgot password command.
        /// </summary>
        public Command ForgotPasswordCommand { get; set; }

        /// <summary>
        ///     Gets/sets create account command.
        /// </summary>
        public Command TermsAndConditionsCommand { get; set; }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public LoginPageViewModel(Page page)
        {
            IsBusy = true;

            _page = page;
            LoginCommand = new Command(loginCommand);
            RegisterCommand = new Command(registerCommand);
            ForgotPasswordCommand = new Command(forgotPasswordCommand);
            TermsAndConditionsCommand = new Command(termsAndConditionsCommand);

            IsBusy = false;
        }

        #endregion

        /// <summary>
        ///     Login command.
        /// </summary>
        private async void loginCommand()
        {
            IsBusy = true;

            if (!_acceptedTerms)
            {
                await _page.DisplayAlert("Alert", "You must accept the terms to proceed.", "OK");
                IsBusy = false;
                return;
            }

            var result = await _userController.OAuthentication(_userModel.Username, _userModel.Password);

            if (result)
            {
                await Application.Current.MainPage.Navigation.PushModalAsync(new MainPage());
            }
            else
            {
                await _page.DisplayAlert("Error", "Wrong credentials. This app uses the Integration enviroment. Make sure your account is registered on: integration.familysearch.org", "OK");
            }

            IsBusy = false;
        }

        /// <summary>
        ///     Register command.
        /// </summary>
        private async void registerCommand()
        {
            IsBusy = true;

            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());

            IsBusy = false;
        }

        /// <summary>
        ///     Forgot password command.
        /// </summary>
        private async void forgotPasswordCommand()
        {
            IsBusy = true;

            await Application.Current.MainPage.Navigation.PushAsync(new RecoverPasswordPage());

            IsBusy = false;
        }

        /// <summary>
        ///     Navigate to terms and conditions page.
        /// </summary>
        private void termsAndConditionsCommand()
        {
            IsBusy = true;


            IsBusy = false;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}