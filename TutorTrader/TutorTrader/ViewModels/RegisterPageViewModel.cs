﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Models;
using TutorTrader.Pages;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The Register view model class.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class RegisterPageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;
        private Page _page;
        private User _userModel = new User();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets user information.
        /// </summary>
        public User UserModel
        {
            get => _userModel;
            set => SetProperty(ref _userModel, value);
        }

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public Boolean IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }
        
        /// <summary>
        ///     Gets/sets create account command.
        /// </summary>

        public Command CreateAccountCommand { get; set; }

        /// <summary>
        ///     Gets/sets create account command.
        /// </summary>

        public Command TermsAndConditionsCommand { get; set; }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public RegisterPageViewModel(Page page)
        {
            IsBusy = true;

            CreateAccountCommand = new Command(createAccountCommand);
            TermsAndConditionsCommand = new Command(termsAndConditionsCommand);
            _page = page;

            IsBusy = false;
        }

        #endregion

        /// <summary>
        ///     Create an account command.
        /// </summary>
        private async void createAccountCommand()
        {
            IsBusy = true;

            await _page.DisplayAlert("Alert", "This page is not hooked up, go to integration.familysearch.org to create an account.", "OK");

            IsBusy = false;
        }

        /// <summary>
        ///     Navigate to terms and conditions page.
        /// </summary>
        private void termsAndConditionsCommand()
        {
            IsBusy = true;



            IsBusy = false;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}