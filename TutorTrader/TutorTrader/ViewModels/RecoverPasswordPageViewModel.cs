﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TutorTrader.Controls;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The Register view model class.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class RecoverPasswordPageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;

        private String _usernameOrEmail;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets username or email.
        /// </summary>
        public String UsernameOrEmail
        {
            get => _usernameOrEmail;
            set => SetProperty(ref _usernameOrEmail, value);
        }

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        /// <summary>
        ///     Gets/sets recover password command.
        /// </summary>

        public Command RecoverPasswordCommand { get; set; }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public RecoverPasswordPageViewModel()
        {
            IsBusy = true;

            RecoverPasswordCommand = new Command(recoverPasswordCommand);

            IsBusy = false;
        }

        #endregion

        /// <summary>
        ///     Recover password command.
        /// </summary>
        private void recoverPasswordCommand()
        {
            IsBusy = true;

            System.Threading.Thread.Sleep(5000);
            string message = "Success!";
            
            DependencyService.Get<IMessageControl>().Longtime(message);

            IsBusy = false;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}