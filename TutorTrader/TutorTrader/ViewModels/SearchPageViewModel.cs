﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TutorTrader.Libraries;
using TutorTrader.Model;
using TutorTrader.Model.Search;
using TutorTrader.Pages;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The main page view model class.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class SearchPageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;
        private UserController _userController = UserController.Instance;
        private Page _page;

        private string _givenName;
        private string _surname;
        private string _gender;
        private string _birthPlace;
        private string _deathPlace;
        private string _marriagePlace;
        private string _fatherGivenName;
        private string _fatherSurname;
        private string _fatherBirthPlace;
        private string _motherGivenName;
        private string _motherSurname;
        private string _motherBirthPlace;
        private string _spouseGivenName;
        private string _spouseSurname;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets search command.
        /// </summary>
        public Command SearchCommand { get; set; }

        /// <summary>
        ///     Gets/sets the given name
        /// </summary>
        public string GivenName
        {
            get => _givenName;
            set { _givenName = value; OnPropertyChanged(nameof(GivenName)); }
        }

        /// <summary>
        ///     Gets/sets the surname
        /// </summary>
        public string Surname
        {
            get => _surname;
            set { _surname = value; OnPropertyChanged(nameof(Surname)); }
        }

        /// <summary>
        ///     Gets/sets the gender
        /// </summary>
        public string Gender
        {
            get => _gender;
            set { _gender = value; OnPropertyChanged(nameof(Gender)); }
        }

        /// <summary>
        ///     Gets/sets the birth place
        /// </summary>
        public string BirthPlace
        {
            get => _birthPlace;
            set { _birthPlace = value; OnPropertyChanged(nameof(BirthPlace)); }
        }

        /// <summary>
        ///     Gets/sets the death place
        /// </summary>
        public string DeathPlace
        {
            get => _deathPlace;
            set { _deathPlace = value; OnPropertyChanged(nameof(DeathPlace)); }
        }

        /// <summary>
        ///     Gets/sets the marriage place
        /// </summary>
        public string MarriagePlace
        {
            get => _marriagePlace;
            set { _marriagePlace = value; OnPropertyChanged(nameof(MarriagePlace)); }
        }

        /// <summary>
        ///     Gets/sets the father given name
        /// </summary>
        public string FatherGivenName
        {
            get => _fatherGivenName;
            set { _fatherGivenName = value; OnPropertyChanged(nameof(FatherGivenName)); }
        }

        /// <summary>
        ///     Gets/sets the father surname
        /// </summary>
        public string FatherSurname
        {
            get => _fatherSurname;
            set { _fatherSurname = value; OnPropertyChanged(nameof(FatherSurname)); }
        }

        /// <summary>
        ///     Gets/sets the father birth place
        /// </summary>
        public string FatherBirthPlace
        {
            get => _fatherBirthPlace;
            set { _fatherBirthPlace = value; OnPropertyChanged(nameof(FatherBirthPlace)); }
        }

        /// <summary>
        ///     Gets/sets the mother given name
        /// </summary>
        public string MotherGivenName
        {
            get => _motherGivenName;
            set { _motherGivenName = value; OnPropertyChanged(nameof(MotherGivenName)); }
        }

        /// <summary>
        ///     Gets/sets the mother surname
        /// </summary>
        public string MotherSurname
        {
            get => _motherSurname;
            set { _motherSurname = value; OnPropertyChanged(nameof(MotherSurname)); }
        }

        /// <summary>
        ///     Gets/sets the mother birth place
        /// </summary>
        public string MotherBirthPlace
        {
            get => _motherBirthPlace;
            set { _motherBirthPlace = value; OnPropertyChanged(nameof(MotherBirthPlace)); }
        }

        /// <summary>
        ///     Gets/sets the spouse given name
        /// </summary>
        public string SpouseGivenName
        {
            get => _spouseGivenName;
            set { _spouseGivenName = value; OnPropertyChanged(nameof(SpouseGivenName)); }
        }

        /// <summary>
        ///     Gets/sets the supouse surname
        /// </summary>
        public string SpouseSurname
        {
            get => _spouseSurname;
            set { _spouseSurname = value; OnPropertyChanged(nameof(SpouseSurname)); }
        }

        /// <summary>
        ///     Gets/sets IsBusy
        /// </summary>
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public SearchPageViewModel(Page page)
        {
            IsBusy = true;

            _page = page;
            // retrieveTreeAsync();
            SearchCommand = new Command(searchCommand);

            IsBusy = false;
        }

        #endregion

        public async void retrieveTreeAsync()
        {
            if (await _userController.CurrentTreePerson())
            {

            }
            else
            {
                await _page.DisplayAlert("Error", "Unable to retrieve current person tree.", "OK");
            }
        }

        public Dictionary<string, string> createDictionary(string givenName, string surname, string gender, string birthPlace,
                                             string deathPlace, string marriagePlace,
                                             string fatherGivenName, string fatherSurname, string fatherBirthPlace, string motherGivenName,
                                             string motherSurname, string motherBirthPlace, string spouseGivenName, string spouseSurname)
        {
            return new Dictionary<string, string>()
            {
                { "givenName", givenName},
                { "surname", surname},
                { "gender", gender},
                { "birthPlace", birthPlace},
                { "deathPlace", deathPlace},
                { "marriagePlace", marriagePlace},
                { "fatherGivenName", fatherGivenName},
                { "fatherSurname", fatherSurname},
                { "fatherBirthPlace", fatherBirthPlace},
                { "motherGivenName", motherGivenName},
                { "motherSurname", motherSurname},
                { "motherBirthPlace", motherBirthPlace},
                { "spouseGivenName", spouseGivenName},
                { "spouseSurname", spouseSurname}
            };
        }

        /// <summary>
        ///     Search command.
        /// </summary>
        private async void searchCommand()
        {
            IsBusy = true;

            var result =
            await _userController.PersonSearch(
                    createDictionary(_givenName, _surname, _gender, _birthPlace,
                                        _deathPlace, _marriagePlace,
                                        _fatherGivenName, _fatherSurname, _fatherBirthPlace, _motherGivenName,
                                        _motherSurname, _motherBirthPlace, _spouseGivenName, _spouseSurname), 10
                    );

            await _page.Navigation.PushModalAsync(new SearchResultPage(displayResults(result)));

            IsBusy = false;
        }

        private ObservableCollection<PersonToBeBaptized> displayResults(SearchResponse result)
        {
            List<string> ids = new List<string>();
            var people = new ObservableCollection<PersonToBeBaptized>();
            if (result?.entries == null)
            {
                return null ;
            }
            foreach (var entry in result.entries)
            {
                if (!ids.Contains(entry.id))
                {
                    ids.Add(entry.id);
                    var person = entry?.content?.gedcomx?.persons?[0];
                    string deathDate = null;
                    bool dead = false;
                    string deathPlace = null;
                    if (person?.facts?.Length > 1)
                    {
                        deathDate = person?.facts?[1]?.date?.original;
                        dead = true;
                        deathPlace = person?.facts?[1]?.place?.original;
                    }
                    people.Add(new PersonToBeBaptized()
                    {
                        Name = person?.names?[0]?.nameForms?[0]?.fullText,
                        Baptized = false, // TODO: Check if person was baptized
                        Age = person?.facts?[0]?.date?.original,
                        PersonID = person?.id,
                        Dead = dead,
                        DeathDate = deathDate,
                        BirthPlace = person?.display?.birthPlace,
                        Gender = person?.display?.gender,
                        DeathPlace = deathPlace
                    }
                    );
                }
            }
            
            return people;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}