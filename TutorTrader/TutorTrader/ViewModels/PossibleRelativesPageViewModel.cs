﻿#region Copyright

// ==================================================================================================
//   This file is part of the FamilySearch application.
//   Copyright ©2018 TutorTrader. All rights reserved.
// ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using TutorTrader.Libraries;
using TutorTrader.Model;
using TutorTrader.Model.Acenstry;
using TutorTrader.Model.Search;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    public class PossibleRelativesPageViewModel
    {
        #region Fields

        private UserController _userController = UserController.Instance;
        private Boolean _isBusy;
        private Page _page;
        private ObservableCollection<PersonToBeBaptized> _people;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public Boolean IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public ObservableCollection<PersonToBeBaptized> People
        {
            get => _people;
            set => SetProperty(ref _people, value);
        }

        #endregion

        #region Methods

        #region Constructor

        public PossibleRelativesPageViewModel(Page page)
        {
            IsBusy = true;

            _page = page;

            var task = Task.Run(async () => await retrieveCurrentUser());
            Task.Delay(500);
            People = task.Result;

            IsBusy = false;
        }

        #endregion

        public async Task<ObservableCollection<PersonToBeBaptized>> retrieveCurrentUser()
        {

            if (await _userController.CurrentTreePerson() && await _userController.GetCurrentUserAsync())
            {
                if (_userController.currentUser.gender == "MALE")
                {
                    _userController.currentUser.gender = "Male";
                }
                else if (_userController.currentUser.gender == "FEMALE")
                {
                    _userController.currentUser.gender = "Female";
                }

                await _userController.GetUserAncestry();
                List<string> listOfIds = new List<string>();
                foreach(var person in _userController._ancestryResponse.persons)
                {
                    listOfIds.Add(person.id);
                }
                List<NecessaryPersonInfo> listOfAncestryInfo = new List<NecessaryPersonInfo>();
                foreach(var id in listOfIds)
                {
                    listOfAncestryInfo.Add(await _userController.GetUserNecessaryInformation(id));
                }

                List<SearchResponse> results = new List<SearchResponse>();
                foreach (var person in listOfAncestryInfo)
                {
                    var lastNames = person.lastName.Split(' ');
                    foreach (var lastName in lastNames)
                    {
                        var searchParameters = new Dictionary<string, string>()
                        {
                            { "surname", lastName},
                            { "birthPlace", person.birthPlace}
                        };

                        results.Add(await _userController.PersonSearch(searchParameters, 5));
                    }
                }

                return displayResults(results);

            }
            else
            {
                await _page.DisplayAlert("Error", "Unable to retrieve current user.", "OK");
                return new ObservableCollection<PersonToBeBaptized>();
            }
        }

        private ObservableCollection<PersonToBeBaptized> displayResults(List<SearchResponse> results)
        {
            List<string> ids = new List<string>();
            var people = new ObservableCollection<PersonToBeBaptized>();
            foreach (var result in results)
            {
                if (result?.entries == null)
                {
                    continue;
                }
                foreach(var entry in result.entries)
                {
                    if (!ids.Contains(entry.id))
                    {
                        ids.Add(entry.id);
                        var person = entry?.content?.gedcomx?.persons?[0];
                        string deathDate = null;
                        bool dead = false;
                        string deathPlace = null;
                        if (person?.facts?.Length > 1)
                        {
                            deathDate = person?.facts?[1]?.date?.original;
                            dead = true;
                            deathPlace = person?.facts?[1]?.place?.original;
                        }
                        people.Add(new PersonToBeBaptized()
                        {
                            Name = person?.names?[0]?.nameForms?[0]?.fullText,
                            Baptized = false, // TODO: Check if person was baptized
                            Age = person?.facts?[0]?.date?.original,
                            PersonID = person?.id,
                            Dead = dead,
                            DeathDate = deathDate,
                            BirthPlace = person?.display?.birthPlace,
                            Gender = person?.display?.gender,
                            DeathPlace = deathPlace
                        } 
                        );
                    }
                }
            }
            return people;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
