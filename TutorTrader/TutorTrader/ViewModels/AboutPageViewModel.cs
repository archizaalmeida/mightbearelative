﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The about view model class.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class AboutPageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public Boolean IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        /// <summary>
        ///     Gets/sets email command.
        /// </summary>
        public ICommand EmailCommand
        {
            get;
            set;
        }

        /// <summary>
        ///     Gets/sets facebook command.
        /// </summary>
        public ICommand FacebookCommand
        {
            get;
            set;
        }

        /// <summary>
        ///     Gets/sets call me command.
        /// </summary>
        public ICommand CallMeCommand
        {
            get;
            set;
        }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public AboutPageViewModel()
        {
            IsBusy = true;

            EmailCommand = new Command(emailCommand);
            FacebookCommand = new Command(facebookCommand);
            CallMeCommand = new Command(callMeCommand);

            IsBusy = false;
        }

        #endregion

        /// <summary>
        ///     Send email command.
        /// </summary>
        private void emailCommand(object obj)
        {
            IsBusy = true;

            Device.OpenUri(new Uri("mailto:archiza@hotmail.com"));

            IsBusy = false;
        }

        /// <summary>
        ///     Send facebook command.
        /// </summary>
        private void facebookCommand()
        {
            IsBusy = true;

            Device.OpenUri(new Uri("https://www.facebook.com/marcelo.archiza"));

            IsBusy = false;
        }

        /// <summary>
        ///     Send call me command.
        /// </summary>
        private void callMeCommand()
        {
            IsBusy = true;

            Device.OpenUri(new Uri(String.Format("tel:{0}", "+18018089633")));

            IsBusy = false;
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}