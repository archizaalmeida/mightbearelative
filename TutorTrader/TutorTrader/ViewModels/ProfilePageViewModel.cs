﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TutorTrader.Libraries;
using TutorTrader.Model.UserModel;
using TutorTrader.Properties;
using Xamarin.Forms;

namespace TutorTrader.ViewModels
{
    /// <summary>
    ///     The profile page view model class.
    /// </summary>
    /// <seealso cref="BindableBase" />
    public class ProfilePageViewModel : INotifyPropertyChanged
    {
        #region Fields

        private Boolean _isBusy;
        private Page _page;
        private User _userModel;
        private UserController _userController = UserController.Instance;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets/sets page is busy.
        /// </summary>
        public Boolean IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        /// <summary>
        ///     Gets/sets user information.
        /// </summary>
        public User UserModel
        {
            get => _userModel;
            set { _userModel = value; OnPropertyChanged(nameof(UserModel)); }
        }

        /// <summary>
        ///     Gets/sets user name.
        /// </summary>
        string displayName;
        public string DisplayName
        {
            get => displayName;
            set { displayName = value; OnPropertyChanged(nameof(DisplayName)); }
        }

        #endregion

        #region Methods

        #region Constructors

        /// <summary>
        ///     Default Constructor.
        /// </summary>
        public ProfilePageViewModel(Page page)
        {
            IsBusy = true;

            _page = page;
            retrieveCurrentUser();

            IsBusy = false;
        }

        #endregion

        public async void retrieveCurrentUser()
        {
            if (await _userController.CurrentTreePerson() && await _userController.GetCurrentUserAsync())
            {
                if(_userController.currentUser.gender == "MALE")
                {
                    _userController.currentUser.gender = "Male";
                }
                else if(_userController.currentUser.gender == "FEMALE")
                {
                    _userController.currentUser.gender = "Female";
                }
                UserModel = _userController.currentUser;
            }
            else
            {
                await _page.DisplayAlert("Error", "Unable to retrieve current user.", "OK");
            }
        }

        #endregion

        #region OnPropertyChanged

        /// <summary>
        ///    Sets property.
        /// </summary>
        /// <param name="field">Field variable.</param>
        /// <param name="value">Value variable.</param>
        /// <param name="propertyName">Proerty name.</param>
        /// <param name="onChanged">On changed method.</param>
        protected virtual void SetProperty<T>(ref T field, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return;

            field = value;

            if (onChanged != null) onChanged();

            OnPropertyChanged(propertyName);
        }

        /// <summary>
        ///     Links C# with XAML.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}