﻿

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TutorTrader.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PersonInfoControl
    {
        public PersonInfoControl()
        {
            InitializeComponent();
        }

        /// <summary>
        ///     Gets/sets Name.
        /// </summary>
        public string Name
        {
            get => (string)GetValue(NameProperty);
            set => SetValue(NameProperty, value);
        }

        /// <summary>
        ///     <see cref="Name" /> bindable property.
        /// </summary>
        public static readonly BindableProperty NameProperty =
            BindableProperty.Create(nameof(Name), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateName());

        /// <summary>
        ///     Updates Name.
        /// </summary>
        private void updateName()
        {
            PersonName.Text = Name;
        }

        /// <summary>
        ///     Gets/sets Baptized.
        /// </summary>
        public bool Baptized
        {
            get => (bool)GetValue(BaptizedProperty);
            set => SetValue(BaptizedProperty, value);
        }

        /// <summary>
        ///     <see cref="Baptized" /> bindable property.
        /// </summary>
        public static readonly BindableProperty BaptizedProperty =
            BindableProperty.Create(nameof(Baptized), typeof(bool), typeof(PersonInfoControl), false,
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateBaptized());

        /// <summary>
        ///     Updates Baptized.
        /// </summary>
        private void updateBaptized()
        {
            if (Baptized)
            {
                YesOrNo.Text = "Yes";
                YesOrNo.TextColor = Color.Red;
            }
            else
            {
                YesOrNo.Text = "No";
                YesOrNo.TextColor = Color.LightGreen;
            }
        }

        /// <summary>
        ///     Gets/sets Age.
        /// </summary>
        public string Age
        {
            get => (string)GetValue(AgeProperty);
            set => SetValue(AgeProperty, value);
        }

        /// <summary>
        ///     <see cref="Age" /> bindable property.
        /// </summary>
        public static readonly BindableProperty AgeProperty =
            BindableProperty.Create(nameof(Age), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateAge());

        /// <summary>
        ///     Updates Age.
        /// </summary>
        private void updateAge()
        {
            AgeLabel.Text = Age;
        }

        /// <summary>
        ///     Gets/sets PersonID.
        /// </summary>
        public string PersonID
        {
            get => (string)GetValue(PersonIDProperty);
            set => SetValue(PersonIDProperty, value);
        }

        /// <summary>
        ///     <see cref="PersonID" /> bindable property.
        /// </summary>
        public static readonly BindableProperty PersonIDProperty =
            BindableProperty.Create(nameof(PersonID), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updatePersonID());

        /// <summary>
        ///     Updates PersonID.
        /// </summary>
        private void updatePersonID()
        {
            PersonIDLabel.Text = PersonID;
        }

        /// <summary>
        ///     Gets/sets Dead.
        /// </summary>
        public bool Dead
        {
            get => (bool)GetValue(DeadProperty);
            set => SetValue(DeadProperty, value);
        }

        /// <summary>
        ///     <see cref="Dead" /> bindable property.
        /// </summary>
        public static readonly BindableProperty DeadProperty =
            BindableProperty.Create(nameof(Dead), typeof(bool), typeof(PersonInfoControl), false,
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateDead());

        /// <summary>
        ///     Updates Dead.
        /// </summary>
        private void updateDead()
        {
            if (Dead)
            {
                YesOrNoDeath.Text = "Yes";
                YesOrNoDeath.TextColor = Color.Red;
            }
            else
            {
                YesOrNoDeath.Text = "No";
                YesOrNoDeath.TextColor = Color.LightGreen;
            }
        }

        /// <summary>
        ///     Gets/sets BirthPlace.
        /// </summary>
        public string BirthPlace
        {
            get => (string)GetValue(BirthPlaceProperty);
            set => SetValue(BirthPlaceProperty, value);
        }

        /// <summary>
        ///     <see cref="BirthPlace" /> bindable property.
        /// </summary>
        public static readonly BindableProperty BirthPlaceProperty =
            BindableProperty.Create(nameof(BirthPlace), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateBirthPlace());

        /// <summary>
        ///     Updates BirthPlace.
        /// </summary>
        private void updateBirthPlace()
        {
            BirthPlaceLabel.Text = BirthPlace;
        }

        /// <summary>
        ///     Gets/sets Gender.
        /// </summary>
        public string Gender
        {
            get => (string)GetValue(GenderProperty);
            set => SetValue(GenderProperty, value);
        }

        /// <summary>
        ///     <see cref="Gender" /> bindable property.
        /// </summary>
        public static readonly BindableProperty GenderProperty =
            BindableProperty.Create(nameof(Gender), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateGender());

        /// <summary>
        ///     Updates Gender.
        /// </summary>
        private void updateGender()
        {
            GenderLabel.Text = Gender;
        }

        /// <summary>
        ///     Gets/sets DeathPlace.
        /// </summary>
        public string DeathPlace
        {
            get => (string)GetValue(DeathPlaceProperty);
            set => SetValue(DeathPlaceProperty, value);
        }

        /// <summary>
        ///     <see cref="DeathPlace" /> bindable property.
        /// </summary>
        public static readonly BindableProperty DeathPlaceProperty =
            BindableProperty.Create(nameof(DeathPlace), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateDeathPlace());

        /// <summary>
        ///     Updates DeathPlace.
        /// </summary>
        private void updateDeathPlace()
        {
            DeathPlaceLabel.Text = DeathPlace;
        }

        /// <summary>
        ///     Gets/sets DeathDate.
        /// </summary>
        public string DeathDate
        {
            get => (string)GetValue(DeathDateProperty);
            set => SetValue(DeathDateProperty, value);
        }

        /// <summary>
        ///     <see cref="DeathPlace" /> bindable property.
        /// </summary>
        public static readonly BindableProperty DeathDateProperty =
            BindableProperty.Create(nameof(DeathDate), typeof(string), typeof(PersonInfoControl), default(string),
                propertyChanged:
                (bindable, oldValue, newValue) =>
                    ((PersonInfoControl)bindable).updateDeathDate());

        /// <summary>
        ///     Updates DeathDate.
        /// </summary>
        private void updateDeathDate()
        {
            DeathDateLabel.Text = DeathDate;
        }
    }
}