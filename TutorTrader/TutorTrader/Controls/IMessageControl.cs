﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

namespace TutorTrader.Controls
{
    ///<summary>
    ///    Interface for messages.
    ///</summary>
    public interface IMessageControl
    {
        #region Methods

        void Longtime(string message);
        void ShortTime(string message);

        #endregion
    }
}