﻿#region Copyright
// THIS FILE IS BEING USED FOR TEST. I DO NOT OWN IT AND HAVE NO RIGHTS OVER IT! THIS FILE IS OWNED BY NAVITAIRE AND THEIR CLIENTS.
#endregion

using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using Xamarin.Forms;

namespace TutorTrader.Controls
{
    /// <summary>
    ///     The template stack layout which replaces list views.
    ///     It is more flexible in layouts but is less efficient.
    ///     Using for long lists isn't recommended.
    ///     Created to be able to handle dynamic sizing (i.e. collapsible, etc).
    /// </summary>
    public class TemplateStackLayout : ContentView
    {
        #region Properties

        /// <summary>
        ///     The items source.
        /// </summary>
        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        /// <summary>
        ///     The item template.
        /// </summary>
        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        /// <summary>
        ///     The <see cref="StackLayout" /> orientation.
        /// </summary>
        public StackOrientation Orientation
        {
            get => (StackOrientation)GetValue(OrientationProperty);
            set => SetValue(OrientationProperty, value);
        }

        /// <summary>
        ///     The <see cref="StackLayout" /> spacing between items.
        /// </summary>
        public double Spacing
        {
            get => (double)GetValue(SpacingProperty);
            set => SetValue(SpacingProperty, value);
        }

        #endregion

        #region Bindable Properties

        /// <summary>
        ///     The bindable items source property.
        /// </summary>
        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable), typeof(TemplateStackLayout),
                default(IEnumerable), propertyChanged: onItemsSourceChanged);

        /// <summary>
        ///     The bindable item template property.
        /// </summary>
        public static readonly BindableProperty ItemTemplateProperty =
            BindableProperty.Create(nameof(ItemTemplate), typeof(DataTemplate), typeof(TemplateStackLayout),
                default(DataTemplate));

        /// <summary>
        ///     The <see cref="StackLayout" /> orientation.
        /// </summary>
        public static readonly BindableProperty OrientationProperty =
            BindableProperty.Create(nameof(Orientation), typeof(StackOrientation), typeof(TemplateStackLayout),
                default(StackOrientation));

        /// <summary>
        ///     The <see cref="StackLayout" /> spacing.
        /// </summary>
        public static readonly BindableProperty SpacingProperty = BindableProperty.Create(nameof(Spacing),
            typeof(double), typeof(TemplateStackLayout), StackLayout.SpacingProperty.DefaultValue);

        #endregion

        #region Methods

        /// <summary>
        ///     Called when the items sources has been changed.
        /// </summary>
        /// <param name="bindable">The bindable object.</param>
        /// <param name="value">The value.</param>
        /// <param name="newValue">The new value.</param>
        private static void onItemsSourceChanged(BindableObject bindable, object value, object newValue)
        {
            var layout = bindable as TemplateStackLayout;

            if (layout == null)
            {
                return;
            }

            var items = layout.ItemsSource as INotifyCollectionChanged;
            if (items != null)
            {
                items.CollectionChanged += (sender, args) => { layout.CreateContent(); };
            }

            layout.CreateContent();
        }


        /// <summary>
        ///     Creates the layout content.
        /// </summary>
        public void CreateContent()
        {
            if (ItemTemplate == null || ItemsSource == null)
            {
                Content = null;
                return;
            }

            var layout = new StackLayout
            {
                Orientation = Orientation,
                Padding = 0,
                Margin = 0,
                Spacing = Spacing
            };

            var itemsSourceCount = 0;
            foreach (var item in ItemsSource)
            {
                itemsSourceCount++;

                var itemTemplateContent = (View)ItemTemplate.CreateContent();
                if (itemTemplateContent is BindableObject bindableObject)
                {
                    bindableObject.BindingContext = item;
                }

                itemTemplateContent.BindingContext = item;
                layout.Children.Add(itemTemplateContent);
            }

            if (itemsSourceCount != 0 && layout.Children.Count == 0)
            {
                Debug.WriteLine("If your ItemsSource is not populating in your TemplateStackLayout" +
                                " be sure to wrap your DataTemplate content in a ViewCell as follows:\n" +
                                "\t<TemplateStackLayout.ItemTemplate>\n" +
                                "\t\t<DataTemplate>\n" +
                                "\t\t\t<ViewCell>\n" +
                                "\t\t\t\t...your template...\n" +
                                "\t\t\t</ViewCell>\n" +
                                "\t\t</DataTemplate>\n" +
                                "\t</TemplateStackLayout.ItemTemplate>\n");
            }

            Content = layout;
        }

        #endregion
    }
}