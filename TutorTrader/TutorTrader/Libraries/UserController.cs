﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using TutorTrader.Model.Acenstry;
using TutorTrader.Model.Search;
using TutorTrader.Model.UserModel;

namespace TutorTrader.Libraries
{
    public sealed class UserController
    {
        private static UserController instance = null;
        private string _url = "https://integration.familysearch.org/";
        private string _grant_type = "password";
        private string _client_id = "a023Z00000Xqz9nQAB";
        private OAuthResponse _oAuthResponse = new OAuthResponse();
        public User currentUser = new User();
        public AncestryResponse _ancestryResponse;

        public static UserController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserController();
                }
                return instance;
            }
        }

        private UserController()
        {

        }

        public async Task<bool> OAuthentication(string username, string password)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(_url);
            var parameters = new Dictionary<string, string> {
                    { "username", username },
                    { "password", password },
                    { "grant_type", _grant_type },
                    { "client_id", _client_id } };

            var encodedContent = new FormUrlEncodedContent(parameters);

            HttpResponseMessage response = await client.PostAsync("cis-web/oauth2/v3/token", encodedContent).ConfigureAwait(false);

            var responseString = await response.Content.ReadAsStringAsync();
            _oAuthResponse = JsonConvert.DeserializeObject<OAuthResponse>(responseString);

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> CurrentTreePerson()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;

            using (HttpClient client = new HttpClient(httpClientHandler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var url = "https://integration.familysearch.org/platform/tree/current-person";

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _oAuthResponse.access_token);

                var response = await client.GetAsync(url);

                if(response.StatusCode == HttpStatusCode.SeeOther)
                {
                    response = await client.GetAsync(response.Headers.Location);
                }
                var responseString = await response.Content.ReadAsStringAsync();

                return response.IsSuccessStatusCode;
            }
        }

        public async Task<bool> GetCurrentUserAsync()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;

            using (HttpClient client = new HttpClient(httpClientHandler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var url = "https://integration.familysearch.org/platform/users/current";

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _oAuthResponse.access_token);

                var response = await client.GetAsync(url);

                if (response.StatusCode == HttpStatusCode.SeeOther)
                {
                    response = await client.GetAsync(response.Headers.Location);
                }

                var responseString = await response.Content.ReadAsStringAsync();
                var users = JsonConvert.DeserializeObject<UsersModel>(responseString);
                currentUser = users.users[0];

                return response.IsSuccessStatusCode;
            }
        }

        public async Task<SearchResponse> PersonSearch(Dictionary<string, string> parameters, int resultAmount)
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;

            using (HttpClient client = new HttpClient(httpClientHandler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-gedcomx-atom+json"));

                var query = "";
                foreach (var par in parameters)
                {
                    if (!string.IsNullOrWhiteSpace(par.Value))
                    {
                        query += String.Format("q.{0}={1}&", par.Key, par.Value);
                    }
                }

                var url = "https://integration.familysearch.org/platform/tree/search?" + query + "offset=0&count=" + resultAmount;

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _oAuthResponse.access_token);


                var response = await client.GetAsync(url);

                if (response.StatusCode == HttpStatusCode.SeeOther)
                {
                    response = await client.GetAsync(response.Headers.Location);
                }

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Replace("https://integration.familysearch.org/platform/places/1322658", "http");
                responseString.Replace("$", "urn");
                var ancestryResponse = JsonConvert.DeserializeObject<SearchResponse>(responseString);

                return ancestryResponse;
            }
        }

        // DO NOT CALL BEFORE GETTING THE CURRENT USER
        public async Task<bool> GetUserAncestry()
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;

            using (HttpClient client = new HttpClient(httpClientHandler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-fs-v1+json"));
                var url = "https://integration.familysearch.org/platform/tree/ancestry?person=" + currentUser.personId + "&generations=8";

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _oAuthResponse.access_token);

                var response = await client.GetAsync(url);

                if (response.StatusCode == HttpStatusCode.SeeOther)
                {
                    response = await client.GetAsync(response.Headers.Location);
                }

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Replace("http://gedcomx.org/Persistent", "http");
                responseString.Replace("change-history", "changehistory");
                responseString.Replace("non-matches", "nonmatches");
                responseString.Replace("source-descriptions", "sourcedescriptions");
                responseString.Replace("http://familysearch.org/v1/ChildAndParentsRelationship", "href");
                var ancestryResponse = JsonConvert.DeserializeObject<AncestryResponse>(responseString);
                _ancestryResponse = ancestryResponse;

                return response.IsSuccessStatusCode;
            }
        }

        public async Task<NecessaryPersonInfo> GetUserNecessaryInformation(string id)
        {
            HttpClientHandler httpClientHandler = new HttpClientHandler();
            httpClientHandler.AllowAutoRedirect = false;

            using (HttpClient client = new HttpClient(httpClientHandler))
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/x-fs-v1+json"));
                var url = "https://integration.familysearch.org/platform/tree/persons/" + id;

                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _oAuthResponse.access_token);

                var response = await client.GetAsync(url);

                if (response.StatusCode == HttpStatusCode.SeeOther)
                {
                    response = await client.GetAsync(response.Headers.Location);
                }

                var responseString = await response.Content.ReadAsStringAsync();
                responseString.Replace("http://gedcomx.org/Persistent", "http");
                responseString.Replace("http://gedcomx.org/Primary", "http");
                responseString.Replace("change-history", "changehistory");
                responseString.Replace("non-matches", "nonmatches");
                responseString.Replace("source-descriptions", "sourcedescriptions");
                responseString.Replace("http://familysearch.org/v1/ChildAndParentsRelationship", "href");
                var ancestryResponse = JsonConvert.DeserializeObject<AncestryResponse>(responseString);
                _ancestryResponse = ancestryResponse;
                var lastName = _ancestryResponse?.persons[0]?.names[0]?.nameForms[0]?.parts[1]?.value;
                var birthPlace = _ancestryResponse?.persons[0]?.display?.birthPlace;
                return new NecessaryPersonInfo(lastName, birthPlace);
            }
        }

        public void Clear()
        {
            _oAuthResponse = new OAuthResponse();
        }
    }
}
