﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TutorTrader.Libraries
{
    class OAuthResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string token { get; set; }
    }
}
