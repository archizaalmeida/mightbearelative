﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Android.Content;
using TutorTrader.Droid.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(SpecialEntryRenderer))]

namespace TutorTrader.Droid.Renderer
{
    class SpecialEntryRenderer : EntryRenderer
    {
        #region Methods

        #region Constructors

        public SpecialEntryRenderer(Context context) : base(context)
        {
        }

        #endregion

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // TODO: Change all entries
            }
        }

        #endregion
    }
}