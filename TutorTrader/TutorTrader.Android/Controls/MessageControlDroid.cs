﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using Android.Widget;
using TutorTrader.Controls;
using TutorTrader.Droid.Controls;

[assembly: Xamarin.Forms.Dependency(typeof(MessageControlDroid))]

namespace TutorTrader.Droid.Controls
{
    ///<summary>
    ///    Class that creates toast messages on Android.
    ///</summary>
    class MessageControlDroid : IMessageControl
    {
        #region Methods

        ///<summary>
        ///    Toast message with long time.
        ///</summary>
        public void Longtime(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }

        ///<summary>
        ///    Toast message with short time.
        ///</summary>
        public void ShortTime(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short).Show();
        }

        #endregion
    }
}