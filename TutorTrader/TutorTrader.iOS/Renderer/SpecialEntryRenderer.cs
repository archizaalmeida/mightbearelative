﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using TutorTrader.Controls;
using TutorTrader.iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(SpecialEntry), typeof(SpecialEntryRenderer))]

namespace TutorTrader.iOS.Renderer
{
    public class SpecialEntryRenderer : EntryRenderer
    {
        #region Methods

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // TODO: Change all entries
            }
        }

        #endregion
    }
}