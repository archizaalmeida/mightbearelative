﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using TutorTrader.Effects.Entries;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(HideUnderlineEffect), nameof(HideUnderlineEffect))]

namespace TutorTrader.iOS.Effects.Entries
{
    class HideUnderlineEffect : PlatformEffect
    {
        #region Methods

        ///<summary>
        ///    Override on attached method.
        ///</summary>
        protected override void OnAttached()
        {
            if (Control is UITextField control)
            {
                control.BorderStyle = UITextBorderStyle.None;
            }
        }

        ///<summary>
        ///    Override on detached method.
        ///</summary>
        protected override void OnDetached()
        {
            if (Control is UITextField control)
            {
                control.BorderStyle = default(UITextBorderStyle);
            }
        }

        #endregion
    }
}