﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion


using System;
using Foundation;
using TutorTrader.Effects.Labels;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName(TutorTrader.Effects.EffectsGroupName.ResolutionGroupName)]
[assembly: ExportEffect(typeof(UnderlineEffect), nameof(UnderlineEffect))]

namespace TutorTrader.iOS.Effects.Labels
{
    public class UnderlineEffect : PlatformEffect
    {
        #region Methods

        ///<summary>
        ///    Override on attached method.
        ///</summary>
        protected override void OnAttached()
        {
            setUnderline(true);
        }

        ///<summary>
        ///    Override on detached method.
        ///</summary>
        protected override void OnDetached()
        {
            setUnderline(false);
        }

        ///<summary>
        ///    Override on element property changed method.
        ///</summary>
        protected override void OnElementPropertyChanged(System.ComponentModel.PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);

            if (args.PropertyName == Label.TextProperty.PropertyName ||
                args.PropertyName == Label.FormattedTextProperty.PropertyName)
            {
                setUnderline(true);
            }
        }

        ///<summary>
        ///    Sets underline on labels.
        ///</summary>
        private void setUnderline(bool underlined)
        {
            try
            {
                var label = (UILabel)Control;
                var text = (NSMutableAttributedString)label.AttributedText;
                var range = new NSRange(0, text.Length);

                if (underlined)
                {
                    text.AddAttribute(UIStringAttributeKey.UnderlineStyle,
                        NSNumber.FromInt32((int)NSUnderlineStyle.Single), range);
                }
                else
                {
                    text.RemoveAttribute(UIStringAttributeKey.UnderlineStyle, range);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}