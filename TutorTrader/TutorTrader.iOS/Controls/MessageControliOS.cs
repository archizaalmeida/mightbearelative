﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the FamilySearch application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using GlobalToast;
using TutorTrader.Controls;

namespace TutorTrader.iOS.Controls
{
    class MessageControliOS : IMessageControl
    {
        public void Longtime(string message)
        {
            Toast.MakeToast(message).SetDuration(2).Show();
        }

        public void ShortTime(string message)
        {
            Toast.MakeToast(message).SetDuration(0.5).Show();
        }
    }
}