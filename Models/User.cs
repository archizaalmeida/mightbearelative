﻿#region Copyright

// // ==================================================================================================
// //   This file is part of the TutorTrader application.
// //   Copyright ©2018 TutorTrader. All rights reserved.
// // ==================================================================================================

#endregion

using System;

namespace Models
{
    public class User
    {
        #region Properties

        public String Username { get; set; }
        public String Password { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }

        #endregion
    }
}